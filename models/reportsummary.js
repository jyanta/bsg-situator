/**
 * Created by jyant on 5/12/2017.
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var AssignmentValue = new Schema({
    assignee: {
        department_id: String,
        role_id: String,
        shift_id: String,
        unit_id: String
    },
    sender_id: String,
    current: Boolean,
    received: Number,
    sent: Number,
    timeless_start: {type: Number, default: null}
});

var ReportItemValue = new Schema({
    _id: String,
    value: Schema.Types.Mixed,
    items: [ { type: Schema.Types.Object, ref : 'ReportItemValue' }]
});

var PhotoValue = new Schema({
    width: Number,
    height: Number,
    url: String,
    thumbnail_url: String
});

var RSSchema = new Schema({
        _id: String,
        agency_id: {type: String, required: true},
        report_schema_id: {type: String, required: true},
        display_title: {type: String, required: true},
        title: String,
        assignments: [AssignmentValue],
        coordinate: {
            accuracy: Number,
            latitude: Number,
            longitude: Number,
            received: Number
        },
        coordinate_error: String,
        description: String,
        location: String,
        occurred: Number,
        photos: [PhotoValue],
        received: Number,
        reporter_contact_allowed: Boolean,
        reporter_email: String,
        reporter_name: String,
        reporter_phone: String,
        responder_report_state_id: {type: String, default: null},
        sent: { type: Number, required: true },
        state: {type: String, default: null},
        status_id: {type: String, default: null},
        started: { type: Number, required: true },
        tag: {type: String, default: null},
        user_id: {type: String, required: true},
        type: String,
        inserted_time: Number,
        external_identifiers: {
            situator_id: String
        },
        chat_summary: {
            user_last_received: Number,
            dashboard_last_received: Number,
            status: String
        },
        note_summary: {
            dashboard_last_received: Number
        },
        internal_message_summary: {
            last_received: Number
        }
});

module.exports = mongoose.model('ReportSummary', RSSchema);