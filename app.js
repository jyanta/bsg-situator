/**
 * Created by jyant on 8/22/2017.
 */
const express = require('express');        // call express
const app = express();                 // define our app using express
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const regexp = require('node-regexp');
//const request = require('request-promise');
const request = require('request');
const http = require('http');
const jwt = require('express-jwt');
const jsonfile = require('jsonfile');
const moment = require('moment');
require('dotenv').config();

app.use(bodyParser.json());
//app.use('/changes', changes);

//Pull in variables from Environment file
var tokenURL = process.env.CLOUD_LOGIN_URL;
var tokenBody = process.env.CLOUD_LOGIN_BODY;
var bsgGetRSURL = process.env.GET_RS_URL;
var bsgUpdateRSURL = process.env.UPDATE_RS_URL;

//Ports
var port = 1111;

//Data file
var file = 'data.json';
var mfile = 'message.json';

function HumanDate(date) {
    return moment(date).format('MM/DD/YYYY HH:mm:ss');
}

app.get('/', function(req,res) {
    console.log("You are connected");
    res.json({"message": "You are connected"});
});

//Call to Cloud to get back Data
app.get('/rs', function(req,res){
    console.log('start situator pull');

    var options = { method: 'POST',
        //url: 'https://bsg-testing.auth0.com/oauth/token',
        url: tokenURL,
        headers: { 'content-type': 'application/json' },
        //body: '{"client_id":"AqWjh3SGrNTbhGkUofC6EqSDlXfRKR13","client_secret":"SkOVPIXXjE2Cm_VPvHiW9i3K_3Jt7Qbq9rJrip9ZJ4v0H_ScbfBKCDNjWzLIp3nb","audience":"https://thebrassstargroup.com/iOS","grant_type":"client_credentials"}' };
        body: tokenBody};

    var acctoke = {};

    //console.log(options);

    request(options, function (error, response, body) {
        if (error) throw new Error(error);

        var jsondata = jsonfile.readFileSync(file);
        var setTime = jsondata.inserted_time || 0;
        //console.log(setTime);

        acctoke = JSON.parse(body);
        //console.log(acctoke.access_token);

        console.log("After JSON parse of AccessToken");
        var options2 = { method: 'GET',
            //url: 'http://bsgbackend-dev.herokuapp.com/changes/agencies/594650907884dd0011d952a2/report_summaries?type=created&inserted_after=' + setTime,
            url: bsgGetRSURL + setTime,
            headers: { authorization: 'Bearer ' + acctoke.access_token }};

        //console.log(options2);

        request(options2, function (error, response, body2) {
            if (error) throw new Error(error);
            //console.log(body2);

            var sitBody = JSON.parse(body2);
            //console.log(sitBody.inserted_time);
            //console.log("writing to JSON.data");

            var obj = {inserted_time: sitBody.inserted_time};
            jsonfile.writeFile(file, obj, function (err) {
                console.error(err);
            });
            //console.log(sitBody.value);

            if (sitBody.values.length > 0) {
                console.log("Messages in the call.  Call situator");
                //res.json(sitBody);
                sitBody.values.forEach(function (report_summary) {
                    //SITUATOR CODE
                    var sit_body = {
                        "IncidentID": "-1",
                        "IncidentType": "5220",
                        "Name": report_summary.value.report_summary.title + " - BSG-ID: " + report_summary.value.report_summary.display_title,
                        "Description": report_summary.value.report_summary.description,
                        "Severity": "Normal",
                        "IncidentGroupID": "12",
                        "X": report_summary.value.report_summary.coordinate.longitude,
                        "Y": report_summary.value.report_summary.coordinate.latitude,
                        "Location": report_summary.value.report_summary.location
                    };

                    const options3 = {
                        //url: 'http://10.1.2.42:80/SituatorWebAPI/api/login',
                        url: process.env.SITUATOR_URL,
                        method: 'POST',
                        headers: {
                            'Accept': 'application/json',
                            'Authorization': 'Basic anVzdGlueTpxd2Vkc2E='
                        }
                    };

                    //res.json(sit_body);
                    request(options3, function (err, resp) {
                        if(!resp.headers['set-cookie']){
                            console.log("Bad Header");
                            return json.body({message:"Bad Header"});
                        } else {
                            var setcookie = resp.headers['set-cookie'];
                            if (setcookie) {
                                setcookie.forEach(
                                    function (cookiestr) {
                                        //console.log(setcookie);
                                        var istart = 0;
                                        var iend = cookiestr.indexOf(";");
                                        var subSes = cookiestr.substr(istart, iend - istart);

                                        const options4 = {
                                            //url: 'http://10.1.2.42:80/SituatorWebApi/odata/Incidents',
                                            url: process.env.SITUATOR_INCIDENTS_URL,
                                            method: 'POST',
                                            headers: {
                                                'Accept': 'application/json',
                                                'Cookie': subSes
                                            },
                                            body: sit_body,
                                            json: true
                                        };

                                        //console.log(options4);

                                        request(options4, function (err, resp2) {
                                            if (err) {
                                                res.status(404).json(err + "        " + resp2);
                                            } else {
                                                //console.log(resp2);
                                                //res.json(resp2);
                                                console.log("Situator Incident ID: " + resp2.body.IncidentID);
                                                //console.log(report_summary.value);
                                                //Create variables for calling back to Report Summary

                                                if(report_summary.value.report_summary.coordinate.longitude){
                                                    const optionMap = {
                                                        url: process.env.SITUATOR_MAP_URL + "(" + resp2.body.IncidentID + ")/PlaceIncidentOnAllGeoMaps",
                                                        method: 'POST',
                                                        headers: {
                                                            'Accept': 'application/json',
                                                            'Cookie': subSes
                                                        }
                                                    };

                                                    request(optionMap, function(err, mapResp){
                                                        //console.log(mapResp);
                                                    })
                                                }

                                                var sitID = resp2.body.IncidentID;
                                                var rsID = report_summary.value._id;

                                                var contactRep;
                                                if(report_summary.value.reporter_contact_allowed==true){
                                                    contactRep = "Yes"
                                                } else {
                                                    contactRep = "No"
                                                }

                                                var fOccured = null;
                                                var fReceived = null;
                                                var fStarted = null;

                                                if(report_summary.value.report_summary.occurred){
                                                    //var dOccured = new Date(report_summary.value.report_summary.occurred);
                                                    //fOccured = (dOccured.getMonth() + 1) + "/" + dOccured.getDate() + "/" + dOccured.getFullYear() + " " + dOccured.getHours() + ":" + dOccured.getMinutes() + ":" + dOccured.getSeconds();

                                                    fOccured = HumanDate(report_summary.value.report_summary.occurred);
                                                }

                                                if(report_summary.value.report_summary.received){
                                                    //var dReceived = new Date(report_summary.value.report_summary.received);
                                                    //fReceived = (dReceived.getMonth() + 1) + "/" + dReceived.getDate() + "/" + dReceived.getFullYear() + " " + dReceived.getHours() + ":" + dReceived.getMinutes() + ":" + dReceived.getSeconds();

                                                    fReceived = HumanDate(report_summary.value.report_summary.received);
                                                }

                                                if(report_summary.value.report_summary.started){
                                                    //var dStarted = new Date(report_summary.value.report_summary.started);
                                                    //fStarted = (dStarted.getMonth() + 1) + "/" + dStarted.getDate() + "/" + dStarted.getFullYear() + " " + dStarted.getHours() + ":" + dStarted.getMinutes() + ":" + dStarted.getSeconds();

                                                    fStarted = HumanDate(report_summary.value.report_summary.started);
                                                }

                                                var sit_ex_body = [
                                                    {"IncidentID":resp2.body.IncidentID,"Name":"TransitWatch_ReportID","Category":"AddedByForm","Type":"String","Value":report_summary.value.report_summary._id},
                                                    {"IncidentID":resp2.body.IncidentID,"Name":"TransitWatch_AgencyID","Category":"AddedByForm","Type":"String","Value":report_summary.value.report_summary.agency_id},
                                                    {"IncidentID":resp2.body.IncidentID,"Name":"TransitWatch_ReportSchemaID","Category":"AddedByForm","Type":"String","Value":report_summary.value.report_summary.report_schema_id},
                                                    {"IncidentID":resp2.body.IncidentID,"Name":"TransitWatch_Title","Category":"AddedByForm","Type":"String","Value":report_summary.value.report_summary.title},
                                                    {"IncidentID":resp2.body.IncidentID,"Name":"TransitWatch_DisplayTitle","Category":"AddedByForm","Type":"String","Value":report_summary.value.report_summary.display_title},
                                                    {"IncidentID":resp2.body.IncidentID,"Name":"TransitWatch_Description","Category":"AddedByForm","Type":"String","Value":report_summary.value.report_summary.description},
                                                    {"IncidentID":resp2.body.IncidentID,"Name":"TransitWatch_Location","Category":"AddedByForm","Type":"String","Value":report_summary.value.report_summary.location},
                                                    {"IncidentID":resp2.body.IncidentID,"Name":"TransitWatch_DateOcurred","Category":"AddedByForm","Type":"String","Value":fOccured},
                                                    {"IncidentID":resp2.body.IncidentID,"Name":"TransitWatch_DateReceived","Category":"AddedByForm","Type":"String","Value":fReceived},
                                                    {"IncidentID":resp2.body.IncidentID,"Name":"TransitWatch_DateStarted","Category":"AddedByForm","Type":"String","Value":fStarted},
                                                    {"IncidentID":resp2.body.IncidentID,"Name":"TransitWatch_ReporterName","Category":"AddedByForm","Type":"String","Value":report_summary.value.report_summary.reporter_name},
                                                    {"IncidentID":resp2.body.IncidentID,"Name":"TransitWatch_ReporterEMail","Category":"AddedByForm","Type":"String","Value":report_summary.value.report_summary.reporter_email},
                                                    {"IncidentID":resp2.body.IncidentID,"Name":"TransitWatch_ReporterPhone","Category":"AddedByForm","Type":"String","Value":report_summary.value.report_summary.reporter_phone},
                                                    {"IncidentID":resp2.body.IncidentID,"Name":"TransitWatch_ReporterUserID","Category":"AddedByForm","Type":"String","Value":report_summary.value.report_summary.user_id},
                                                    {"IncidentID":resp2.body.IncidentID,"Name":"TransitWatch_ReporterContactAllowed","Category":"AddedByForm","Type":"String","Value":contactRep},
                                                    {"IncidentID":resp2.body.IncidentID,"Name":"TransitWatch_Status","Category":"AddedByForm","Type":"String","Value":"New"}
                                                ];

                                                if(report_summary.value.report_summary.photos){
                                                    var arrayPoint = 1;
                                                    report_summary.value.report_summary.photos.forEach(function(photo){
                                                        sit_ex_body.push({"IncidentID":resp2.body.IncidentID,"Name":"TransitWatch_PhotoURL_" + arrayPoint,"Category":"AddedByForm","Type":"String","Value":photo.url});
                                                        arrayPoint ++
                                                    });
                                                }

                                                console.log(sit_ex_body);

                                                //Expanded Situator
                                                var options5 = {
                                                    url: process.env.SITUATOR_EXTENDED_URL,
                                                    method: 'POST',
                                                    headers: {
                                                        'Accept': 'application/json',
                                                        'Cookie': subSes
                                                    },
                                                    body: sit_ex_body,
                                                    json: true
                                                };

                                                request(options5, function(err, resp3){
                                                    if (err) {
                                                        res.status(404).json(err + "        " + resp3);
                                                    } else {
                                                        console.log("Call Backend to save ID");
                                                        var options6 = { method: 'PUT',
                                                            //url: 'http://bsgbackend-dev.herokuapp.com/changes/report_summaries/'+ rsID +'/updateSituator/' + sitID,
                                                            url: bsgUpdateRSURL + rsID + '/updateSituator/' + sitID,
                                                            headers: { authorization: 'Bearer ' + acctoke.access_token },
                                                            body: '' };

                                                        request(options6, function (error, response, body6) {
                                                            if (error) throw new Error(error);

                                                            //console.log("Successfully Saved Report Summary: " + rsID)
                                                            //console.log(body6);
                                                        })
                                                    }
                                                })
                                            }
                                        });
                                    }
                                )
                            }
                        }
                    })
                })
            } else {
                console.log("No Messages");
                return res.status(200).json({'message': 'No data sent to Situator'});
            }
        });
    });
});

app.get('/messages', function(req, res){
   //This is so you can get messages for Situator
    console.log('start situator message alert');

    var options = { method: 'POST',
        //url: 'https://bsg-testing.auth0.com/oauth/token',
        url: tokenURL,
        headers: { 'content-type': 'application/json' },
        //body: '{"client_id":"AqWjh3SGrNTbhGkUofC6EqSDlXfRKR13","client_secret":"SkOVPIXXjE2Cm_VPvHiW9i3K_3Jt7Qbq9rJrip9ZJ4v0H_ScbfBKCDNjWzLIp3nb","audience":"https://thebrassstargroup.com/iOS","grant_type":"client_credentials"}' };
        body: tokenBody};

    var acctoke = {};

    request(options, function (error, response, body) {
        if (error) throw new Error(error);

        var jsondata = jsonfile.readFileSync(mfile);
        var setTime = jsondata.inserted_time || 0;
        //console.log(setTime);

        acctoke = JSON.parse(body);

        console.log("After JSON parse of AccessToken");
        var options2 = {
            method: 'GET',
            //url: 'http://bsgbackend-dev.herokuapp.com/changes/agencies/594650907884dd0011d952a2/report_summaries?type=created&inserted_after=' + setTime,
            url: process.env.GET_MESSAGE_URL + setTime,
            headers: {authorization: 'Bearer ' + acctoke.access_token}
        };

        //console.log(options2);

        request(options2, function (error, response, body2) {
            if (error) throw new Error(error);

            var sitBody = JSON.parse(body2);

            var obj = {inserted_time: sitBody.inserted_time};
            jsonfile.writeFile(mfile, obj, function (err) {
                console.error(err);
            });
            //console.log(sitBody.value);

            if (sitBody.values.length > 0) {
                console.log("Messages in the call.  Update situator");

                sitBody.values.forEach(function(report_summary){
                const options3 = {
                    //url: 'http://10.1.2.42:80/SituatorWebAPI/api/login',
                    url: process.env.SITUATOR_URL,
                    method: 'POST',
                    headers: {
                        'Accept': 'application/json',
                        'Authorization': 'Basic anVzdGlueTpxd2Vkc2E='
                    }
                };

                //res.json(sit_body);
                request(options3, function (err, resp) {
                    if(!resp.headers['set-cookie']){
                        console.log("Bad Header");
                        return json.body({message:"Bad Header"});
                    } else {
                        var setcookie = resp.headers['set-cookie'];
                        if (setcookie) {
                            setcookie.forEach(
                                function (cookiestr) {
                                    //console.log(setcookie);
                                    var istart = 0;
                                    var iend = cookiestr.indexOf(";");
                                    var subSes = cookiestr.substr(istart, iend - istart);
                                    var sit_message = '';
                                    var sit_body = [];
                                    var incidentID = report_summary.change_info.external_identifiers.situator_id;

                                    console.log("Situator Incident ID: " + incidentID);
                                    if(report_summary.value.messages){
                                        report_summary.value.messages.forEach(function (message){
                                            //var dOccured = new Date(message.received);
                                            //var fOccured = (dOccured.getMonth() + 1) + "/" + dOccured.getDate() + "/" + dOccured.getFullYear() + " " + dOccured.getHours() + ":" + dOccured.getMinutes() + ":" + dOccured.getSeconds();

                                            var fOccured = HumanDate(message.received);

                                            if(message.type==report_summary.change_info.subtype){
                                                sit_message = sit_message + fOccured + ' - ' + message.display_name + ': ' + message.message + '\r\n';
                                            }
                                        });
                                    }

                                    //console.log(sit_message);

                                    if(report_summary.change_info.subtype == 'chat'){
                                        sit_body = [
                                            {"IncidentID":incidentID,"Name":"TransitWatch_NewChats","Category":"AddedByForm","Type":"Boolean","Value":true},
                                            {"IncidentID":incidentID,"Name":"TransitWatch_Alert","Category":"AddedByForm","Type":"Boolean","Value":true},
                                            {"IncidentID":incidentID,"Name":"TransitWatch_Chats","Category":"AddedByForm","Type":"String","Value":sit_message}
                                        ];
                                    } else {
                                        sit_body = [
                                            {"IncidentID":incidentID,"Name":"TransitWatch_NewNotes","Category":"AddedByForm","Type":"Boolean","Value":true},
                                            {"IncidentID":incidentID,"Name":"TransitWatch_Alert","Category":"AddedByForm","Type":"Boolean","Value":true},
                                            {"IncidentID":incidentID,"Name":"TransitWatch_Notes","Category":"AddedByForm","Type":"String","Value":sit_message}
                                        ];
                                    }

                                    var options5 = {
                                        url: process.env.SITUATOR_EXTENDED_URL,
                                        method: 'POST',
                                        headers: {
                                            'Accept': 'application/json',
                                            'Cookie': subSes
                                        },
                                        body: sit_body,
                                        json: true
                                    };

                                    request(options5, function(err, resp3){
                                        if (err) {
                                            res.status(404).json(err + "        " + resp3);
                                        } else {
                                            console.log('Situator Updated');
                                        }
                                    });
                                }
                            )
                        }
                    }
                })
                })
            }
        })
    })
});

// START THE SERVER
// =============================================================================
app.listen(port);
console.log('Magic happens on port ' + port);